#include <stdlib.h>
#include <stdio.h>
#include<stdbool.h>
#include "graphe.h"
#include "file.h"


pfile_t creer_file ()
{
  pfile_t a=(pfile_t)malloc(sizeof(file_t));
  a ->tete=0;
  a->queue=0;
  if (a!=NULL)
    return a;

  
  return NULL ;
}

int detruire_file (pfile_t f)
{
  free(f);
  f=NULL;
  return 1;
  
  
}




int file_vide (pfile_t f)
{
  if (f->tete==f->queue)
    return 1;
  else  
  return 0 ;
}

int file_pleine (pfile_t f){
  if (f->tete < f->queue){
    if(f->queue-f->tete==MAX_FILE_SIZE-1){
      return 1;
    }else return 0;
 
  }else{
  if (f->tete > f->queue){
    if(f->tete-f->queue==1){
      return 1;
    }else
      return 0;
  }}
  return 0;
}

pgraphe_t defiler (pfile_t f)
  {
   if (file_vide(f)) return NULL;
   pgraphe_t result = f->Tab[f->tete];
   f->Tab[f->tete]=NULL;
   int m;
   m=f->tete+1;
   f->tete=m % MAX_FILE_SIZE;
  return  result;
}

int enfiler (pfile_t f, pgraphe_t p)
{
  int m;
  if (file_pleine(f)) return 0;
  f-> Tab[f->queue]=p;
  m=f->queue+1;
  f->queue=m % MAX_FILE_SIZE;
 
  return 1;
}
