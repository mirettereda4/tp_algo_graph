/*
  Structures de type graphe
  Structures de donnees de type liste
  (Pas de contrainte sur le nombre de noeuds des  graphes)
*/


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "graphe.h"
#include "file.h"
#include "pile.h"
#include "fap.h"




psommet_t chercher_sommet (pgraphe_t g, int label)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL) && (s->label != label))
    {
      s = s->sommet_suivant ;
    }
  return s ;
}




parc_t existence_arc (parc_t l, psommet_t s)
{
  parc_t p = l ;

  while (p != NULL)
    {
      if (p->dest == s)
	return p ;
      p = p->arc_suivant ;
    }
  return p ;
  
}




void ajouter_arc (psommet_t o, psommet_t d, int distance)
{
  parc_t parc ;

  parc = (parc_t) malloc (sizeof(arc_t)) ;

  if (existence_arc (o->liste_arcs, d) != NULL)
    {
      fprintf(stderr, "ajout d'un arc deja existant\n") ;
      exit (-1) ;
    }
  
  parc->poids = distance ;
  parc->dest = d ;
  parc->arc_suivant = o->liste_arcs ;
  o->liste_arcs = parc ;
  return ;
}




// ===================================================================



int nombre_sommets (pgraphe_t g)
{
  psommet_t p = g ;
  int nb = 0 ;

  while (p != NULL)
    {
      nb = nb + 1 ;
      p = p->sommet_suivant ;
    }

  return nb ;
}




int nombre_arcs (pgraphe_t g)
{
  psommet_t p = g ;
  int nb_arcs = 0 ;

  while (p != NULL)
    {
      parc_t l = p->liste_arcs ;

      while (l != NULL)
	{
          nb_arcs = nb_arcs + 1 ;
	  l = l->arc_suivant ;
	}
      
      p = p->sommet_suivant ;
    }
    
  return nb_arcs ;
}




void init_couleur_sommet (pgraphe_t g)
{
  psommet_t p = g ;

  while (p != NULL)
    {
      p->couleur = 0 ;           // couleur indefinie
      p = p->sommet_suivant ;    // passer au sommet suivant dans le graphe
    }
  
  return ;
}




int colorier_graphe (pgraphe_t g)
{
  /*
    coloriage du graphe g
    
    datasets
    graphe data/gr_planning
    graphe data/gr_sched1
    graphe data/gr_sched2
  */

  psommet_t p = g ;
  parc_t a ;
  int couleur ;
  int max_couleur = INT_MIN ; // -INFINI
  
  int change ;

  init_couleur_sommet (g) ;
  
  while (p != NULL)
    {
      couleur = 1 ; // 1 est la premiere couleur

      // Pour chaque sommet, on essaie de lui affecter la plus petite couleur

      // Choix de la couleur pour le sommet p
      
      do
	{
	  a = p->liste_arcs ;
	  change = 0 ;
      
	  while (a != NULL)
	    {
	      if (a->dest->couleur == couleur)
		{
		  couleur = couleur + 1 ;
		  change = 1 ;
		} 
	      a = a->arc_suivant ; 
	    }

	} while (change == 1) ;

      // couleur du sommet est differente des couleurs de tous les voisins
      
      p->couleur = couleur ;
      if (couleur > max_couleur)
	max_couleur = couleur ;

      p = p->sommet_suivant ;
    }
  
  return max_couleur ;
}




psommet_t premier_non_visite (pgraphe_t g)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL) && (s->visite==1))
    {
      s = s->sommet_suivant ;
    }
  return s ;
}




void reset_visite(pgraphe_t g)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL))
    {
      s->visite=0;
      s = s->sommet_suivant ;
    }
  
}




void afficher_graphe_largeur (pgraphe_t g, int r)
{
  pfile_t file=creer_file();
  psommet_t p=chercher_sommet(g,r);
  if (p==NULL){
  	printf("label non trouvee dans le graphe\n");
  	return;
  }
  enfiler(file,p);
  
  while (!file_vide(file)){
  	psommet_t s=defiler(file);
  	if (s->visite==0){
  		printf(" %d ",s->label);
  		s->visite=1;
  	
  	}
  	parc_t arcs=s->liste_arcs;
  	while(arcs!=NULL){
  		if (arcs->dest->visite==0){
  			enfiler(file,arcs->dest);
  		}
  		arcs=arcs->arc_suivant;
  	}
  	if (file_vide(file)){
	  	psommet_t n=premier_non_visite(g);
	  	if (n!=NULL){
	  		enfiler (file,n);
	  	}
  	}
  	
  }
  reset_visite(g);
 
  return ;
}




void afficher_graphe_profondeur (pgraphe_t g, int r)
{
  ppile_t pile=creer_pile();
  psommet_t p=chercher_sommet(g,r);
  
  if (p==NULL){
  	printf("label non trouvee dans le graphe\n");
  	return;
  }
  empiler(pile,p);
  
  while (!pile_vide(pile)){
  	psommet_t s=depiler(pile);
  	if (s->visite==0){
  		printf(" %d ",s->label);
  		s->visite=1;
  	}
  	
  	parc_t arcs=s->liste_arcs;
  	
  	while(arcs!=NULL){
  		if (arcs->dest->visite==0){
  			empiler(pile,arcs->dest);
  		}
  		arcs=arcs->arc_suivant;
  	}
  	
  	if (pile_vide(pile)){
	  	psommet_t n=premier_non_visite(g);
	  	if (n!=NULL){
	  		empiler (pile,n);
	  	}
  	}	
  }
  
  reset_visite(g);
  
  return ;
}




void init_Dijkstra(pgraphe_t g , fap* f ,psommet_t sommet){
	psommet_t s ;

	s = g ;

	while ((s!= NULL))
	    {
	    if (s!=sommet){
	     	s->distance=INT_MAX;
	     }
	     s->pere=NULL;
	     inserer(f, s, s->distance);
	    // printf("%d\n", est_fap_vide(*f));
	      s = s->sommet_suivant ;
	    }	
}




void relachement_arc (psommet_t u, parc_t arc){
	
		if (arc->dest->distance>u->distance+arc->poids){
			arc->dest->distance=u->distance+arc->poids;
			arc->dest->pere=u;
		}
		
}




void print_Dijkstra(pgraphe_t g){
	printf("le resultat de l'algorthme de Dijkstra\n");
	psommet_t s ;

  	s = g ;

  	while ((s!= NULL))
   	 {
      		printf("le sommet %d ,la distance %d , le pere %d \n",s->label , s->distance,(s->pere==NULL)?s->label:s->pere->label);
      		s = s->sommet_suivant ;
    	}	
}




void algo_dijkstra (pgraphe_t g, int r)
{
  fap f=creer_fap_vide();
 // printf("est fap_vide()=%d\n",est_fap_vide(f));
  psommet_t p=chercher_sommet(g,r);
  
  if (p==NULL){
  	printf("label non trouvee dans le graphe\n");
  	return;
  }
  
  p->distance=0;
  init_Dijkstra(g,&f,p);
  
  while (!est_fap_vide(f)){
  	psommet_t courant;
  	int prio;
  	extraire(&f,&courant,& prio);
  	
  	//printf(" extraire %d \n",courant ->label);
  	parc_t arcs= courant->liste_arcs;
  	
  	while (arcs!=NULL){
  		relachement_arc(courant,arcs);
  		arcs=arcs->arc_suivant;		
  	}	
  }
  
  print_Dijkstra(g);
  
  return ;
}




// ======================================================================




int degre_sortant_sommet (pgraphe_t g, psommet_t s)
{
  /*
    Cette fonction retourne le nombre d'arcs sortants 
    du sommet n dans le graphe g
  */ 

  psommet_t n = chercher_sommet (g, s->label) ;
  
  if (n == NULL)
     return -1 ;
     
  else 
  {
     int nb_arcs_sortants = 0 ;
     
     parc_t l = n->liste_arcs ;

     while (l != NULL)
     {
        nb_arcs_sortants = nb_arcs_sortants + 1 ;
	l = l->arc_suivant ;
     }
	
     return nb_arcs_sortants ; 
  }   
}




int degre_entrant_sommet (pgraphe_t g, psommet_t s)
{
  /*
    Cette fonction retourne le nombre d'arcs entrants 
    dans le noeud n dans le graphe g
  */ 

  psommet_t n = chercher_sommet (g, s->label) ;
  
  if (n == NULL)
     return -1 ;
     
  else
  {
     psommet_t p = g ;
     int nb_arcs_entrants = 0 ;

     while (p != NULL)
     {
         parc_t l = p->liste_arcs ;

         while (l != NULL)
	 {
	    if (l->dest->label == s->label)
	    {
                nb_arcs_entrants = nb_arcs_entrants + 1 ;
            }
            
	    l = l->arc_suivant ;
	 }
      
         p = p->sommet_suivant ;
     }
     
     return nb_arcs_entrants ;
  }   
}




int degre_maximal_graphe (pgraphe_t g)
{
  /*
    Max des degres des sommets du graphe g
  */
 int degreMax = -1;
  int degre = 0;
  psommet_t s = g;
  while (s!=NULL) {
    degre = degre_sortant_sommet(g, s) + degre_entrant_sommet(g, s);
    s = s->sommet_suivant;
    if (degreMax < degre) degreMax = degre;
  }
  return degreMax;
}




int degre_minimal_graphe (pgraphe_t g)
{
  /*
    Min des degres des sommets du graphe g
  */
  int degreMin = -1;
  int degre = 0;
  psommet_t s = g;
  while (s!=NULL) {
    degre = degre_sortant_sommet(g, s) + degre_entrant_sommet(g, s);
    s = s->sommet_suivant;
    if (degreMin > degre || degreMin==-1) degreMin = degre;
  }
  return degreMin;
}




int independant (pgraphe_t g)
{
  /* Les aretes du graphe n'ont pas de sommet en commun */
  psommet_t p=g;
  while (p!=NULL){
  	parc_t arcs=p->liste_arcs;
  	int c=0;
  	while (arcs!=NULL){
  		c++;
  		arcs=arcs->arc_suivant;
  	}
  	if (c>1){
  		return 0;
  	}
  	p=p->sommet_suivant;	
  }

  return 1 ;
}




int existe_arc(psommet_t u,psommet_t v){
	parc_t arcs=u->liste_arcs;
	while (arcs!=NULL){
		if (arcs->dest==v){
			return 1;
		}
		arcs=arcs->arc_suivant;	
	}
	arcs=v->liste_arcs;
	while (arcs!=NULL){
		if (arcs->dest==u){
			return 1;
		}
		arcs=arcs->arc_suivant;
	}
	return 0;
}




int complet (pgraphe_t g)
{
  /* Toutes les paires de sommet du graphe sont jointes par un arc */
  
  psommet_t i=g;
  while (i!=NULL){
  	psommet_t j=g;
  	while (j!=NULL){
  		if (i!=j&&!existe_arc(i,j)){
  			return 0;
  		}
  		j=j->sommet_suivant;
  	}
  	i=i->sommet_suivant;
  	
  }

  return 1 ;
}




int regulier (pgraphe_t g)
{
  /* 
     graphe regulier: tous les sommets ont le meme degre
     g est le ponteur vers le premier sommet du graphe
     renvoie 1 si le graphe est régulier, 0 sinon
  */
  return (degre_maximal_graphe(g) != degre_minimal_graphe(g)) ? 0 : 1;
}




/*
  placer les fonctions de l'examen 2017 juste apres
*/




void init_visiter (pgraphe_t g)
{
	psommet_t p = g ;
	
	while (p != NULL) {

		parc_t arcs = p->liste_arcs ;
		
		while (arcs != NULL) {
			arcs->visiter = 0 ;
			arcs = arcs->arc_suivant ;
		}
		
		p = p->sommet_suivant ;
	}
}




int eulerien ( pgraphe_t g , chemin_t c ){
	
	init_visiter(g);
	pchemin_t ch=&c;
	
	
	while (ch!=NULL){
		psommet_t p=chercher_sommet(g,ch->sommet_depart->label);
		if (p==NULL){
			return 0;
		}
		else {
			parc_t arcs=p->liste_arcs;
			while (arcs!=NULL){
				if (arcs->poids==ch->arc->poids&&arcs->dest->label==ch->sommet_dest->label){
					arcs->visiter=1;
				}
				arcs=arcs->arc_suivant;
			}
			ch=ch->chemin_suivant;
			}
		}
	psommet_t i=g;
	while (i!=NULL){
		parc_t arcs=i->liste_arcs;
		while (arcs!=NULL){
			if (arcs->visiter==0){
				return 0;
			}
			arcs=arcs->arc_suivant;
		}
		i=i->sommet_suivant;
	}
	return 1 ;	
}




int graphe_eulerien_aider (psommet_t p,pchemin_t ch,pchemin_t chemin_courant,pgraphe_t g){
	if (p==NULL){
		return 0;
	}
	
	else {  
		parc_t arcs=p->liste_arcs;
		int boolean=0;
		while (arcs!=NULL){
			if (arcs->visiter==0){
				chemin_courant->sommet_dest=arcs->dest;
				chemin_courant->arc=arcs;
				init_visiter(g);
				if (eulerien(g,*ch)==1){
					return 1;
				}
				else {
					//nouveau chemin
					//update nouveau chemin 
					chemin_courant->chemin_suivant=(pchemin_t)malloc(sizeof(chemin_t));
					chemin_courant->chemin_suivant->sommet_depart=arcs->dest;
					boolean=boolean|| graphe_eulerien_aider(arcs->dest,ch,chemin_courant->chemin_suivant,g);
					chemin_courant->chemin_suivant=NULL;
				}
			}
			arcs=arcs->arc_suivant;		
			
		}
		return boolean;
	}
	
}




int graphe_eulerien ( pgraphe_t g ){
	psommet_t p=g;
	while (p!=NULL){
		pchemin_t ch=(pchemin_t)malloc(sizeof(chemin_t));
		ch->sommet_depart=p;
		
		if (graphe_eulerien_aider(p,ch,ch,g)==1){
			return 1;
		}
		p=p->sommet_suivant;
	}
	return 0;
	
}




int simple (pgraphe_t g, chemin_tt c) {
  int label1;
  int label2;
  int labelJ1;
  int labelJ2;
  for (int i=0;i< c->nbSommets-1;i++) {
    label1 = (c->labelSommets[i])->label;
    label2 = (c->labelSommets[i+1])->label;
    for (int j=i+1;j<c->nbSommets-1;j++) {
      labelJ1 = (c->labelSommets[j])->label;
      labelJ2 = (c->labelSommets[j+1])->label;
      if (label1 == labelJ1 && label2 == labelJ2) return 0;
    }
  }
  return 1;
}




//cette fonction je l'ai fait juste pour creer un chemin , c'est pas encore terminé je dois 
//prévoir le retour dasn le cas d'un segmentation fault , je vais le faire si on a le temps mais 
//en géneral elle fonctionne bien si le chemin existe dans le graphe
chemin_tt inputChemin(pgraphe_t g) {
  printf("Veuillez entrer ligne par ligne les labels du chemin\n");
  printf("Entrez F pour mettre fin à l'input\n");
  chemin_tt newChemin = malloc(sizeof(c_t));
  psommet_t * mem;
  newChemin->nbSommets = 0;
  char input[10];
  do {
    scanf("%s", input);
    if (strcmp(input, "F")!=0) {
      newChemin->nbSommets++;
      mem = newChemin->labelSommets;
      newChemin->labelSommets = malloc(sizeof(psommet_t)*newChemin->nbSommets);
      for (int i=0;i<newChemin->nbSommets-1;i++) {
        newChemin->labelSommets[i+1] = mem[i];
      }
      newChemin->labelSommets[0] = chercher_sommet(g, atoi(input));
      free(mem);
      for (int i=0;i<10;i++) input[i]=' ';
    }
  }
  while (strcmp(input, "F")!=0);
  return newChemin;
}




int distance (pgraphe_t g, int x, int y) {
  algo_dijkstra(g,x);
  psommet_t s = chercher_sommet(g,y);
  if (s!=NULL) {
    return s->distance;
  } else {
    return -1;
  }

}




// POUR CETTE FONCTION IL FAUT VRAIMENT RÉGLER DIJKSTRA SINON ÇA VA ME RENDRE TOUJOUR UN GRAND NOMBRE
int excentricite (pgraphe_t g, int n) {
	printf("le resultat de l'algorithme de Dijsktra pour le sommet %d\n",n);
  algo_dijkstra(g,n);
  psommet_t s = g;
  int distanceMax = -1;
  while (s!=NULL) {
    if (s->label != n) {
      if (s->distance > distanceMax) distanceMax = s->distance;
    }
    s = s->sommet_suivant;
  }
  return distanceMax;
}




int diametre ( pgraphe_t g ){
  psommet_t p=g;
  int res=excentricite(g,p->label);
  p=p->sommet_suivant;
  while (p!=NULL){
    int tmp=excentricite(g,p->label);
    if (tmp>res){
      res=tmp;
    }
    p=p->sommet_suivant;

  }
  return res;

}




int elementaire (pgraphe_t g, pchemin_t c)
{
	if (c == NULL)
	{
		return -1 ;
	}	
		
	pchemin_t l = c ;
	psommet_t n ;
	pchemin_t tmp ;
	
	while (l != NULL)
	{
		n = chercher_sommet (g, l->sommet_depart->label) ;
		n->nb_de_visite += 1 ;
		
		if (n->nb_de_visite > 1)
		{
			return 0 ;
		}
		
		tmp = l;
		l = l->chemin_suivant ;
	}
	
	n = chercher_sommet (g, tmp->sommet_dest->label) ;
	n->nb_de_visite += 1 ;
	
	if (n->nb_de_visite > 1)
	{
		return 0 ;
	}
	
	return 1 ;
}




void reset_nb_de_visite (pgraphe_t g)
{
	psommet_t s = g ;

 	while (s != NULL)
    	{
      		s->nb_de_visite = 0 ;
		s = s->sommet_suivant ;
    	}
}




int hamiltonien (pgraphe_t g, pchemin_t c)
{
	if (c == NULL)
	{
		return -1 ;
	}	
	
	int nb_sommets_graphe = nombre_sommets (g) ;
	
	int nb_sommets_chemin = 0 ;
	pchemin_t l = c ;
	psommet_t n ;
	pchemin_t tmp ;
	
	while (l != NULL)
	{
		n = chercher_sommet (g, l->sommet_depart->label) ;
		n->nb_de_visite += 1 ;
		
		if (n->nb_de_visite == 1)
		{
			nb_sommets_chemin += 1 ;
		}
		
		tmp = l;
		l = l->chemin_suivant ;
	}
	
	n = chercher_sommet (g, tmp->sommet_dest->label) ;
	n->nb_de_visite += 1 ;
	
	if (n->nb_de_visite == 1)
	{
		nb_sommets_chemin += 1 ;
	}
	
	if (nb_sommets_chemin == nb_sommets_graphe)
		return 1 ;
	
	else
		return 0 ;
}




int graphe_hamiltonien_aider (psommet_t p, pchemin_t ch, pchemin_t chemin_courant, pgraphe_t g) 
{
	if (p==NULL)
	{
		return 0 ;
	}
	
	else {  
		parc_t arcs = p->liste_arcs ;
		int boolean = 0 ;
		
		while (arcs != NULL) { 
		
			if (arcs->visiter == 0) {
				chemin_courant->sommet_dest = arcs->dest ;
				chemin_courant->arc = arcs ;
				init_visiter(g) ;
				
				if (hamiltonien(g, ch) == 1) {
					return 1 ;
				}
				
				else {
					//nouveau chemin
					//update nouveau chemin 
					chemin_courant->chemin_suivant = (pchemin_t) malloc (sizeof(chemin_t)) ;
					chemin_courant->chemin_suivant->sommet_depart = arcs->dest ;
					boolean = boolean || graphe_hamiltonien_aider (arcs->dest, ch, chemin_courant->chemin_suivant, g) ;
					chemin_courant->chemin_suivant = NULL ;
				}
			}
			
			arcs=arcs->arc_suivant ;		
			
		}
		
		return boolean ;
	}	
}




int graphe_hamiltonien (pgraphe_t g)
{
	psommet_t p = g ;
	
	while (p != NULL)
	{
		pchemin_t ch = (pchemin_t) malloc (sizeof (chemin_t)) ;
		ch->sommet_depart = p ;
		
		if ( graphe_hamiltonien_aider (p, ch, ch, g) == 1)
		{
			return 1 ;
		}
		
		p = p->sommet_suivant ;
	}
	
	return 0 ;
}





























































