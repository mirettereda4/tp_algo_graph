#include <stdio.h>
#include <stdlib.h>

#include "graphe.h"


int main (int argc, char **argv)
{
  pgraphe_t g ;
  int nc ;
  
  if (argc != 2)
    {
      fprintf (stderr, "erreur parametre \n") ;
      exit (-1) ;
    }

  /*
    la fonction lire_graphe alloue le graphe (matrice,...) 
    et lit les donnees du fichier passe en parametre
  */
  
  
  lire_graphe (argv [1], &g) ;

  /*
    la fonction ecrire_graphe affiche le graphe a l'ecran
  */  
  
  printf ("nombre de sommets du graphe %d nombre arcs %d \n", nombre_sommets (g), nombre_arcs (g)) ;
  fflush (stdout) ;
  
  ecrire_graphe (g) ;      

  nc = colorier_graphe (g) ;
  
  printf ("nombre chromatique graphe = %d\n", nc) ;

  ecrire_graphe_colorie (g) ;
  
  printf("afficher graphe largeur \n");
  printf("le depart=1\n");
  afficher_graphe_largeur(g,1);
  printf("\n");
  printf("---------------\n");
  printf("afficher graphe profondeur \n");
  printf("le depart=1\n");
  afficher_graphe_profondeur(g,1);
  printf("\n");
   printf("---------------\n");
  printf("le test de l'algorithme de Dijkstra\n");
  printf("le depart=1\n");
  algo_dijkstra(g,1);
  printf("---------------\n");
  printf("le teste du graphe independant\n");
  int x=independant(g);
  printf("%d\n",x);
  printf("---------------\n");
  printf("le teste du graphe complet\n");
  x=complet(g);
  printf("%d\n",x);
  printf("---------------\n");
  printf("le teste du chemin eulerien\n");
  
  psommet_t a1=(psommet_t)malloc(sizeof(sommet_t));
  a1->label=0;
  psommet_t a2=(psommet_t)malloc(sizeof(sommet_t));
  a2->label=2;
  parc_t a3=(parc_t)malloc(sizeof(arc_t));
  a3->poids=3;
  a3->dest=a2;
  pchemin_t a=(pchemin_t)malloc(sizeof(chemin_t));
  a->sommet_depart=a1;
  a->sommet_dest=a2;
  a->arc=a3;
  
  psommet_t b1=(psommet_t)malloc(sizeof(sommet_t));
  b1->label=2;
  psommet_t b2=(psommet_t)malloc(sizeof(sommet_t));
  b2->label=8;
  parc_t b3=(parc_t)malloc(sizeof(arc_t));
  b3->poids=3;
  b3->dest=b2;
  pchemin_t b=(pchemin_t)malloc(sizeof(chemin_t));
  b->sommet_depart=b1;
  b->sommet_dest=b2;
  b->arc=b3;
  
  psommet_t ch1=(psommet_t)malloc(sizeof(sommet_t));
  ch1->label=8;
  psommet_t ch2=(psommet_t)malloc(sizeof(sommet_t));
  ch2->label=5;
  parc_t c3=(parc_t)malloc(sizeof(arc_t));
  c3->poids=2;
  c3->dest=ch2;
  pchemin_t c=(pchemin_t)malloc(sizeof(chemin_t));
  c->sommet_depart=ch1;
  c->sommet_dest=ch2;
  c->arc=c3;
  
  
  psommet_t d1=(psommet_t)malloc(sizeof(sommet_t));
  d1->label=5;
  psommet_t d2=(psommet_t)malloc(sizeof(sommet_t));
  d2->label=0;
  parc_t d3=(parc_t)malloc(sizeof(arc_t));
  d3->poids=8;
  d3->dest=d2;
  pchemin_t d=(pchemin_t)malloc(sizeof(chemin_t));
  d->sommet_depart=d1;
  d->sommet_dest=d2;
  d->arc=d3;
  
  c->chemin_suivant=d;
  b->chemin_suivant=c;
  a->chemin_suivant=b;
  
  
  
  
  
  printf("le teste\n");
  int r=eulerien(g,*a);
  printf("%d\n",r);
  
  printf("--------------------\n");
  printf("le test du graphe eulerien\n");
  
  x=graphe_eulerien (g);
  printf(" le resultat=%d\n",x);
  printf("--------------------\n");
  
  
  
  
  
  
  printf("\n\n\n======      Test de la fonction degre_sortant_sommet      ======") ;
  int n = degre_sortant_sommet (g, g) ;
  printf("\n\nLe nombre d'arcs sortants du sommet %d dans le graphe g est : %d\n", g->label, n) ;
  
  printf("\n\n\n======      Test de la fonction degre_entrant_sommet      ======") ;
  n = degre_entrant_sommet (g, g->sommet_suivant->sommet_suivant) ;
  printf("\n\nLe nombre d'arcs entrants dans le sommet %d dans le graphe g est : %d\n\n\n", g->sommet_suivant->sommet_suivant->label, n) ;
  
  
  pchemin_t c1 = (pchemin_t) malloc (sizeof (chemin_t)) ;
  pchemin_t c2 = (pchemin_t) malloc (sizeof (chemin_t)) ;
  c1->sommet_depart = g ;
  c1->sommet_dest = g->liste_arcs->dest ;
  c1->chemin_suivant = c2 ;
  c2->sommet_depart = g->liste_arcs->dest ;
  psommet_t s = chercher_sommet (g, g->liste_arcs->dest->label) ;
  c2->sommet_dest = s->liste_arcs->dest ;
  
  
  c2->chemin_suivant = NULL ;
  printf("\n======      Test de la fonction elementaire      ======") ;
  int e = elementaire (g, c1) ;
  if (e == 1)
  	printf("\n\nLe chemin %d->%d->%d est bien un chemin elementaire !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label) ;
  else
  	printf("\n\nLe chemin %d->%d->%d n'est pas un chemin elementaire !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label) ;
  
  reset_nb_de_visite (g) ;
  
  printf("\n======      Test de la fonction hamiltonien      ======") ;
  int h = hamiltonien (g, c1) ;
  if (h == 1)
  	printf("\n\nLe chemin %d->%d->%d est bien un chemin hamiltonien !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label) ;
  else
  	printf("\n\nLe chemin %d->%d->%d n'est pas un chemin hamiltonien !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label) ;
  
  
  /*
  pchemin_t c3 = (pchemin_t) malloc (sizeof (chemin_t)) ;
  c2->chemin_suivant = c3 ;
  c3->sommet_depart = c2->sommet_dest ;
  s = chercher_sommet (g, c3->sommet_depart->label) ;
  c3->sommet_dest = s->liste_arcs->dest ;
  c3->chemin_suivant = NULL ;
  
  reset_nb_de_visite (g) ;
  
  printf("\n======      Test de la fonction hamiltonien      ======") ;
  int h = hamiltonien (g, c1) ;
  if (h == 1)
  	printf("\n\nLe chemin %d->%d->%d->%d est bien un chemin hamiltonien !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label, c3->sommet_dest->label) ;
  else
  	printf("\n\nLe chemin %d->%d->%d->%d n'est pas un chemin hamiltonien !\n\n\n", c1->sommet_depart->label, c1->chemin_suivant->sommet_depart->label, c1->chemin_suivant->sommet_dest->label, c3->sommet_dest->label) ;
  */	
  printf("\n======      Test de la fonction graphe_hamiltonien      ======\n") ;	
  int g_h = graphe_hamiltonien (g) ;
  if (g_h == 1)
  	printf("\n\nCe graphe est bien un graphe hamiltonien !\n\n\n") ;
  else
  	printf("\n\nCe graphe n'est pas un graphe hamiltonien !\n\n\n\n") ;	
  
  printf("le test du graphe simple \n");
  chemin_tt cheminTest ; 
  cheminTest = inputChemin(g);
  if(simple(g, cheminTest)) {
      printf("Ce chemin est simple\n");
    } else {
      printf("Ce chemin n'est pas simple\n");
    }
    
  printf("--------------------\n");
  printf("le test du distance graphe \n");
  printf("Sélectionnez deux sommets x et y : (format input : x y)\n");
  int z;
  int y;
  scanf("%d %d", &z, &y);
  int dist = distance(g, z, y);
  printf("La distance entre %d et %d est de : %d\n", z,y,dist);

  printf("--------------------\n");
  printf("le test d'excentricite d'un graphe \n");
  printf("Sélectionnez un sommet pour le calcul d'excentricite\n");
  scanf("%d", &x);
  int excen = excentricite(g, x);
  printf("L'excentricite du sommet %d est de : %d\n", x, excen);
  int di=diametre(g);
  printf("le diametre du graphe = %d\n",di);
  
   printf("--------------------\n");
  printf("le test  d'un graphe régulier  \n");
  
  if (regulier(g)) {
    printf("Le graphe est régulier\n");
  } else {
    printf("Le graphe n'est pas régulier\n");
  }
  printf("--------------------\n");
  printf("le test  des degrés  graphe  \n");
  printf("Degré maximale du graphe : %d\n", degre_maximal_graphe(g));
  printf("Degré minimale du graphe : %d\n", degre_minimal_graphe(g));
  	
}




























