#include"graphe.h"
#include "fap.h"
#include <stdlib.h>
#include <unistd.h>

fap creer_fap_vide() { 
  	return NULL;
   }

fap inserer(fap* f, pgraphe_t  element, int priorite) {
    fap nouveau;
    // courant;
    // precedent;

    /* nouveau maillon */
    nouveau = (fap)malloc(sizeof(struct maillon));
    nouveau->element = element;
    nouveau->priorite = priorite;

    /* insertion en tete */
    //if ((f == NULL) || (priorite < f->priorite)) {
        nouveau->prochain = *f;
       * f = nouveau;
    //}

    /* recherche de la bonne position et insertion */
   /* else {
        precedent = f;
        courant = f->prochain;
        while ((courant != NULL) && (priorite >= courant->priorite)) {
            precedent = courant;
            courant = courant->prochain;
        }
        precedent->prochain = nouveau;
        nouveau->prochain = courant;
    }*/
    return *f;
}

fap extraire(fap* f, pgraphe_t *element, int *priorite) {
    fap courant;
    fap minimum;
    fap precedent;
    fap precedent_min;

    /* extraire le premier element si la fap n'est pas vide */
    if (*f != NULL) {
    	if ((*f)->prochain!=NULL){
		courant = (*f)->prochain;
		precedent=*f;
		minimum=*f;
		while(courant!=NULL){
			if (courant->element->distance<minimum->element->distance){
				minimum=courant;
				precedent_min=precedent;
				}
			precedent=courant;
			courant=courant->prochain;
			
		}
		*element = minimum->element;
		*priorite = minimum->element->distance;
		if (minimum==*f){
			(*f)=(*f)->prochain;
		}
		else {
			precedent_min->prochain=minimum->prochain;
		}
		free(minimum);
	}
	else {
		*element = (*f)->element;
		*priorite = (*f)->element->distance;
		*f=NULL;
		return *f;
	}		
       
        
    }
    return *f;
}

int est_fap_vide(fap f) { return f == NULL; }

void detruire_fap(fap f) {
    pgraphe_t  element;
    int priorite;

    while (!est_fap_vide(f))
        f = extraire(&f, &element, &priorite);
}
